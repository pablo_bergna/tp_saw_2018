Hay 2 formar de ejecutar el programa:

*Ejecutar app como Java app con maven:
	*mvn clean install spring-boot:run
*Ejecutar directo el Jar:
	*java -jar target/pdfConverter-0.0.1-SNAPSHOT.jar

Una vez levantado ejecutar desde Postman o un navegador:

localhost:8080/getPdf?text=textoQueQuieraPasarAPdf