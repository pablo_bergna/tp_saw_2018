package com.swa.tpgrupo4.pdfConverter.controllers;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.*;


@RestController
public class PdfController {

    private static final String PATH = "\\file.pdf";

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView("src/main/resources/backoffice/index.html");
        return mav;
    }

    @RequestMapping(value = "/getPdf", method = RequestMethod.GET,
        produces = MediaType.APPLICATION_PDF_VALUE)
    public @ResponseBody byte[] convertTextToPdf(
            @RequestParam(value = "text", required = true) String text)
            throws IOException {
        String localPath = System.getProperty("user.dir") + PATH;

        if (text.startsWith("%PDF")) {
            //Loading an existing document
            File file = new File(localPath);
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(text);
            bw.close();
        }else{
            PDDocument document = new PDDocument();
            PDPage my_page = new PDPage();
            document.isAllSecurityToBeRemoved();
            document.addPage(my_page);
            PDPageContentStream contentStream = new PDPageContentStream(document, my_page);
            contentStream.beginText();
            contentStream.newLineAtOffset(100, 700);
            contentStream.setFont(PDType1Font.HELVETICA, 24);
            contentStream.showText(text);
            contentStream.endText();
            contentStream.close();
            document.save(localPath);
            document.close();
        }
        InputStream in = new FileInputStream(localPath);
        return IOUtils.toByteArray(in);
    }
}